// ==UserScript==
// @name         Twitch Force new Player
// @namespace    http://gitlab.com/foxbot/
// @version      1.3
// @description  Replaces default twitch player with HTML5 player.
// @author       420foxbot
// @match        http://*.twitch.tv/*
// @grant        none
// @updateURL    https://gitlab.com/foxbot/Twitch5ForAll/raw/master/twitch5.user.js
// @downloadURL  https://gitlab.com/foxbot/Twitch5ForAll/raw/master/twitch5.user.js
// ==/UserScript==

var hostTimeout = 0;

function waitForHost() {
    hostTimeout += 1;
    if (document.getElementById("hostmode")) {
        console.log("Twitch5: Found host!");
        hostTimeout = 100;
        replaceHost();
        setTimeout(removeButton, 5000);
        return;
    }
    else if (hostTimeout > 20)
    {
        return;
    }
    else
    {
        setTimeout(waitForHost, 500);
    }
}

function waitForPlayer() {
    if (document.getElementById("player")) {
        replacePlayer();
    }
    else
    {
        setTimeout(waitForPlayer, 500);
    }
}
function replacePlayer() {
    var full = window.location.href;
    var chan = full.replace("http://www.twitch.tv/","");
    var done = chan.replace("/popout","");
    if (full.indexOf("popout") > -1)
    {
        console.log("Twitch5: Found channel: " + done);
        document.getElementsByClassName("player-container")[0].innerHTML = '<iframe src=\"http://player.twitch.tv/?channel=' + done + '\" height=\"100%\" width=\"100%\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>';
        console.log("Twitch 5: Popout player replaced.");
    }
    else
    {
        console.log("Twitch5: Found channel: " + done);
        document.getElementById("player").innerHTML = '<iframe src=\"http://player.twitch.tv/?channel=' + done + '\" height=\"100%\" width=\"100%\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>';
        console.log("Twitch5: Player replaced.");
    }
    return;
}

function replaceHost() {
    var chan = getAllElementsWithAttribute('data-tt_content');
    var full = 'http://www.twitch.tv/';
    chan.forEach(function(entry){
        if (entry.getAttribute('data-tt_content') == "host_channel") {
            full = entry.getAttribute('href');
        }
    });
    var done = full.replace("http://www.twitch.tv/","");
    console.log("Twitch5: Found host channel: " + done);
    document.getElementById("player").innerHTML = '<iframe src=\"http://player.twitch.tv/?channel=' + done + '\" height=\"100%\" width=\"100%\" frameborder=\"0\" allowfullscreen=\"true\" webkitallowfullscreen=\"true\" mozallowfullscreen=\"true\"></iframe>';
    console.log("Twitch5: Host player replaced.");
    return;
}


function removeButton() {
    document.getElementById("video-playback").setAttribute("data-branding", "false");
    document.getElementById("video-playback").setAttribute("data-showinfo", "false");
    console.log("Button removed!");
}

// Thanks to StackOverflow user 'kevinfahy'
function getAllElementsWithAttribute(attribute)
{
  var matchingElements = [];
  var allElements = document.getElementsByTagName('*');
  for (var i = 0, n = allElements.length; i < n; i++)
  {
    if (allElements[i].getAttribute(attribute) !== null)
    {
      // Element exists with attribute. Add to array.
      matchingElements.push(allElements[i]);
    }
  }
  return matchingElements;
}


waitForPlayer();
setTimeout(removeButton, 5000);
waitForHost();